package com.devcamp.j02_javabasic.s40;

public class WrapperExample {
    public static void autoBoxing() {
        byte bte = 20;
        short sh = 30;
        int it = 40;
        long lng = 50;
        float fat = 60.0F;
        double dbl = 70.0D;
        char ch = 'b';
        boolean bool = true;
        /***
         * // AutoBoxing: Converting primitives into objects
         * AutoBoxing là cơ chế tự động chuyển đổi kiểu dữ liệu nguyên thủy sang object
         * của  wrapper class tương ứng.
         */
        Byte byteobj = bte;
        Short shortobj = sh;
        Integer intobj = it;
        Long longobj = lng;
        Float floatobj = fat;
        Double doubleobj = dbl;
        Character charobj = ch;
        Boolean boolobj = bool;

        System.out.println("--Printing object values (In giá trị của object)--");
        System.out.println("Byte object: " + byteobj);
        System.out.println("Short object: " + shortobj);
        System.out.println("Integer object: " + intobj);
        System.out.println("Long object: " + longobj);
        System.out.println("double object: " + doubleobj);
        System.out.println("character object: " + charobj);
        System.out.println("float object: " + floatobj);
        System.out.println("boolean object: " + boolobj);

    }

    public static void unBoxing(){
        byte bte = 20;
        short sh = 30;
        int it = 40;
        long lng = 50;
        float fat = 60.0F;
        double dbl = 70.0D;
        char ch = 'b';
        boolean bool = false;
        // Autoboxing : converting primitives into objects
        Byte byteobj = bte;
        Short shortobj = sh;
        Integer intobj = it;
        Long longobj = lng;
        Float floatobj = fat;
        Double doubleobj = dbl;
        Character charobj = ch;
        Boolean boolobj = bool;
          /***
         * // UnBoxing: Converting objects to primitives
         * UnBoxing là cơ chế tự động chuyển đổi các object
         * của  wrapper class sang kiểu dữ liệu nguyên thủy  tương ứng.
         */
        byte bytevalue = byteobj;
        short shortvalue = shortobj;
        int intvalue = intobj;
        long longvalue = longobj;
        float floatvalue = floatobj;
        double doublevalue = doubleobj;
        char charvalue = charobj;
        boolean boolvalue = boolobj;

        System.out.println("--Printing primitive values (in giá trị của các Primitive Data Types (Kiểu dữ liệu nguyên thủy)---");
        System.out.println("byte value: " + bytevalue);
        System.out.println("short value: " + shortvalue);
        System.out.println("int value: " + intvalue);
        System.out.println("long value: " + longvalue);
        System.out.println("float value: " + floatvalue);
        System.out.println("double value: " + doublevalue);
        System.out.println("char value: " + charvalue);
        System.out.println("bool value: " + boolvalue);
    
    }

    public static void main (String[] args){
        WrapperExample.autoBoxing();
        WrapperExample.unBoxing();
    }
  
}
